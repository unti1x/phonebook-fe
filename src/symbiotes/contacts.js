import { createSymbiote } from 'redux-symbiote';
import { reject } from 'lodash';


const initialState = {
    contacts: []
};


const symbiotes = {
    loaded: (state, contacts) =>  Object.assign({}, state, { contacts }),
    deleted: (state, id) => Object.assign({}, state, { 
        contacts: reject(state.contacts, (contact) => `${contact.id}` === `${id}`)
    }),
    showAddPhoneModal: (state, id) => Object.assign({}, state, { 
        showAddPhone: true, activeContactId: id
    }),
    hideAddPhoneModal: (state) => Object.assign({}, state, { 
        showAddPhone: false, activeContactId: null
    })
};


export const { actions, reducer } = createSymbiote(
    initialState, 
    symbiotes,
    'CONTACTS'
);
