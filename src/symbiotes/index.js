import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

// import { reducer as root } from './root';
import {reducer as contacts} from './contacts';
import {reducer as contactForm} from './contactForm';

export default () => combineReducers({
  form: formReducer,
  contacts,
  contactForm
});
