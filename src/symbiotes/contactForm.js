import { createSymbiote } from 'redux-symbiote';


const initialState = {
    contact: {},
    saved: false
};


const symbiotes = {
    loaded: (state, contact) =>  Object.assign({}, state, { contact }),
    saved: () => Object.assign({}, initialState, { saved: true }),
    resetState: () => initialState
};


export const { actions, reducer } = createSymbiote(
    initialState, 
    symbiotes,
    'CONTACT_FORM'
);
