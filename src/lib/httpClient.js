import axios from 'axios';

class ApiClient {

    constructor() {
        this.client = axios.create({
            baseURL: 'http://localhost:8000/api/',
            timeout: 1000
        });
    }

    async getContacts(query) {
        const contacts = await this.client.get('contact', {params: {q: query}});
        return contacts.data;
    }

    async getContact(id) {
        const contact = await this.client.get(`contact/${id}`)
        return contact.data;
    }

    async saveContact(contact) {
        const id = contact.id;
        const action = id ? this.client.put : this.client.post;
        const url = 'contact' + (id ? `/${id}` : '');
        const result = await action(url, contact);
        return result;
    }

    async deleteContact(id) {
        const contact = await this.client.delete(`contact/${id}`);
        return contact.data;
    }
    
    async addPhone(id, phone) {
        const result = await this.client.post(`contact/${id}/phones`, phone);
        return result.data;
    }

}

const client = new ApiClient();

export default client;
