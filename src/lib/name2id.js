var nameMemo = {}

function name2id(name = '') {
    return name.replace(/[\W]/g, '_') + '_' + Math.round(Math.random() * 1e6);
}

export default function(name) {
    if(typeof(nameMemo[name]) === 'undefined') {
        nameMemo[name] = name2id(name);
    }

    return nameMemo[name];
};
