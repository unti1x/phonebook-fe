import React from 'react';
import { Form, Col } from 'react-bootstrap';
import propTypes from 'prop-types';
import name2id from '../lib/name2id';
import ValidatedControl from './ValidatedInput';


const LabeledInput = ({
    label, name, id = name2id(name), type = "text", ...props
}) => (
    <Form.Group as={Form.Row}>
        <Form.Label column htmlFor={id} md={2}>
            {label}
        </Form.Label>
        <Col>
            <ValidatedControl name={name} id={id} {...props} />
        </Col>
    </Form.Group>
);

LabeledInput.propTypes = {
    label: propTypes.string.isRequired,
    type: propTypes.string,
    id: propTypes.string,
    props: propTypes.any
};


const LabeledField = ({input, meta, ...props}) => (
    <LabeledInput meta={meta} {...input} {...props} />
);

export default LabeledField;
