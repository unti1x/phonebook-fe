import React from 'react';
import propTypes from 'prop-types';
import { Control } from 'react-bootstrap/Form';

const ValidatedControl = ({ meta, type = "text", ...props }) => (
    <>
        <Control type={type}
            isInvalid={meta && meta.touched && !!meta.error}
            {...props}
        />
        {meta && meta.touched && meta.error &&
            <Control.Feedback type="invalid">
                {meta.error}
            </Control.Feedback>
        }
    </>
);

ValidatedControl.propTypes = {
    meta: propTypes.object,
    type: propTypes.string,
    props: propTypes.any
};

export default ValidatedControl;
