import client from '../lib/httpClient';
import { actions } from '../symbiotes/contactForm';


export function loadContact(id) {
    return (dispatch) => client.getContact(id).then(
        (data) => dispatch(actions.loaded(data))
    );
}

export function saveContact(contact) {
    return (dispatch) => client.saveContact(contact).then(
        () => dispatch(actions.saved())
    )
}