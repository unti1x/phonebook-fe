import client from '../lib/httpClient';
import { actions } from '../symbiotes/contacts';


export function loadContacts() {
    return (dispatch) => client.getContacts().then(
        (data) => dispatch(actions.loaded(data))
    );
}

export function deleteContact(id) {
    return (dispatch) => client.deleteContact(id).then(
        () => dispatch(actions.deleted(id))
    );
}

export function addPhone(id, phone) {
    return (dispatch) => client.addPhone(id, phone)
        .then( () => client.getContacts() )
        .then( (contacts) => {
            dispatch(actions.loaded(contacts));
            dispatch(actions.hideAddPhoneModal());
        } );
}
