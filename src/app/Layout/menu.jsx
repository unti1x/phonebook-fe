import React from 'react';

import { Link } from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Search from './search';

export default () => (
    <Navbar>
        <Navbar.Brand href="#home">Phonebook</Navbar.Brand>
        <Navbar.Toggle/>
        <Navbar.Collapse>
            <Nav className="mr-auto">
                <Nav.Link as={Link} to="/">
                    Contacts
                </Nav.Link>
                <Nav.Link as={Link} to="/new">
                    Add new
                </Nav.Link>
            </Nav>
        </Navbar.Collapse>
        <Search />
    </Navbar>
);
