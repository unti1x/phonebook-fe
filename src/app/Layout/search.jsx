import React from 'react';
import { Link } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Dropdown from 'react-bootstrap/Dropdown';
import Autocomplete from 'react-autocomplete';

import client from '../../lib/httpClient';


const renderSearch = (props) => (
    <Form.Control {...props} />
);


const renderMenu = (children) => (
    <Dropdown navbar show={children.length > 0} {...console.log(children)}>
        <Dropdown.Menu>{children}</Dropdown.Menu>
    </Dropdown>
);


const renderItem = (item) => (
    <Dropdown.Item as={Link} key={item.id} to={`/contact/${item.id}`}>
        {item.name}
    </Dropdown.Item>
);

class Search extends React.Component {
    
    state = {contacts: [], value: ''};
    
    queryElements = (e, value) => {
        this.setState({value})
        client.getContacts(value).then(
            (contacts) => this.setState({contacts})
        );
    }
    
    render() {
        const {value, contacts} = this.state;

        return (
            <Form inline>
                <Autocomplete renderInput={renderSearch}
                    getItemValue={item => item.id}
                    onChange={this.queryElements}
                    items={contacts || []}
                    renderMenu={renderMenu}
                    renderItem={renderItem}
                    value={value}
                />
            </Form>
        );
    }
    
}

export default Search;
