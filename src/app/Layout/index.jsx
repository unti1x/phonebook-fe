import React from 'react';
import Container from 'react-bootstrap/Container';
import Menu from './menu';

export default ({children}) => (
    <Container>
        <Menu />
        {children}
    </Container>
);
