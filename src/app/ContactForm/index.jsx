import React from 'react';
import { connect } from 'react-redux';
import { Redirect, withRouter } from 'react-router-dom';
import { Form, reduxForm, Field, FieldArray } from 'redux-form';
import { FormGroup, Button } from 'react-bootstrap';

import LabeledField from '../../components/LabeledInput';
import ContactFields from './contactFields';
import { loadContact, saveContact } from '../../thunks/contactForm'
import { actions } from '../../symbiotes/contactForm';


class ContactForm extends React.Component {

    constructor(props) {
        super(props);

        if(props.contactId) {
            props.loadContact(props.contactId);
        }
    }
    
    componentWillUnmount() {
        this.props.resetState();
    }

    render() {
        const {handleSubmit, loading, saved} = this.props;

        if(loading) {
            return 'loading';
        }

        if(saved) {
            return <Redirect push to="/" />;
        }

        return (
            <Form onSubmit={handleSubmit}>
                <Field component={LabeledField} name="name" label="Name" />
                <FieldArray name="phones" component={ContactFields} />

                <FormGroup>
                    <Button type="submit" variant="primary">
                        Save contact
                    </Button>
                </FormGroup>
            </Form>
        );
    }
}


const form = reduxForm({
    form: 'contact',
    onSubmit: (state, dispatch) => dispatch(saveContact(state)),
    enableReinitialize: true
})(ContactForm);

export default withRouter(connect(
    ({contactForm}, {match: {params}}) => ({
        initialValues: contactForm.contact,
        contactId: params.id || null,
        loading: params.id && !contactForm.contact.id,
        saved: contactForm.saved
    }),
    (dispatch) => ({
        loadContact: (id) => dispatch(loadContact(id)),
        resetState: () => dispatch(actions.resetState())
    })
)(form));