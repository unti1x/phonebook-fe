import React from 'react';
import { Field } from 'redux-form';

import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import {Group, Label, Row} from 'react-bootstrap/Form';

import LabeledField from '../../components/LabeledInput';


const Contact = ({item, index, fields}) => (
    <Group>
        <Field component={LabeledField} 
            name={`${item}.number`} 
            label="Number"
        />
        <Field component={LabeledField} 
            name={`${item}.comment`} 
            label="Comment"
        />
        <Button variant="outline-danger" 
            onClick={() => fields.remove(index)}
        >
            - Remove phone
        </Button>
    </Group>
);


const renderContact = (fields) => fields.map(
    (item, index) => <Contact key={index} fields={fields} item={item} index={index} />
);



export default ({fields}) => (
    <Row>
        <Label column md={2}>Contacts</Label>
        <Col>
            <Button variant="success" 
                className="mb-4"
                onClick={() => fields.push({})}
            >
                + Add phone
            </Button>
            { renderContact(fields) }
        </Col>
    </Row>
);
