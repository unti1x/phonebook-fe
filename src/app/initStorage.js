import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import createRootReducer from '../symbiotes';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default (preloadedState => (
    createStore(
        createRootReducer(),
        preloadedState,
        composeEnhancers(
        applyMiddleware(thunk)
        )
    )
));
