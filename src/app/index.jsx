import React from 'react';
import { Provider, connect } from 'react-redux';
import { Route, Switch, HashRouter } from 'react-router-dom';

import configureStore from './initStorage';

import Layout from './Layout';
import Contacts from './Contacts';
import ContactForm from './ContactForm';

const store = configureStore();

const Application = () => (
    <HashRouter>
        <Layout>
            <Switch>
                <Route exect path="/new" component={ContactForm} />
                <Route path="/contact/:id" component={ContactForm} />
                <Route exect path="/" component={Contacts} />
            </Switch>
        </Layout>
    </HashRouter>
);

const ConnectedApp = connect(
    (state) => ({ store })
)(Application);

export default () => (
    <Provider store={store}>
        <ConnectedApp />
    </Provider>
);
