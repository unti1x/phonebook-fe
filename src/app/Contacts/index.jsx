import React, {Fragment} from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';

import List from './contactsList';
import Contact, { EmptyItem } from './contact';
import PhoneModal from './phoneModal';
import { loadContacts } from '../../thunks/contacts';


class Contacts extends React.Component {

    static propTypes = {
        contacts: propTypes.array
    };

    static defaultProps = {
        contacts: []
    };

    constructor(props) {
        super(props);

        props.loadContacts();
    }

    render() {
        const {contacts, showAddPhone, activeContactId} = this.props;

        if (contacts.length === 0) {
            return <EmptyItem />;
        }

        return (
            <Fragment>
                <PhoneModal show={showAddPhone} contactId={activeContactId} />
                <List>
                    { contacts.map((item, index) => <Contact key={index} {...item} />) }
                </List>
            </Fragment>
        );
    }

}


export default connect(
    ({contacts}) => contacts,
    (dispatch) => ({
        loadContacts: () => dispatch(loadContacts())
    })
)(Contacts);