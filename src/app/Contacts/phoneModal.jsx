import React from 'react';
import { connect } from 'react-redux';

import { Form, reduxForm, Field } from 'redux-form';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

import { actions } from '../../symbiotes/contacts';
import { addPhone } from '../../thunks/contacts';
import LabeledField from '../../components/LabeledInput';

const PhoneModal = ({show, contactId, handleClose, handleSubmit, onSubmit}) => (
    <Modal show={show} onHide={handleClose}>
        <Form onSubmit={handleSubmit(
            (values, dispatch) => onSubmit(contactId, values, dispatch)
        ) }>
            <Modal.Header>
                <Modal.Title>Add phone</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Field component={LabeledField} label="Number" name="number" />
                <Field component={LabeledField} label="Comment" name="comment" />
            </Modal.Body>
            <Modal.Footer>
                <Button type="submit" variant="primary">
                    Save
                </Button>
            </Modal.Footer>
        </Form>
    </Modal>
);

const form = reduxForm({
    form: 'contact_phone',
    onSubmit: (id, state, dispatch) => dispatch(addPhone(id, state))
})(PhoneModal);

export default connect(
    null,
    (dispatch) => ({
        handleClose: () => dispatch(actions.hideAddPhoneModal()),
    })
)(form);