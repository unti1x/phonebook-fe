import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Alert from 'react-bootstrap/Alert';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button'

import { deleteContact } from '../../thunks/contacts';
import { actions } from '../../symbiotes/contacts';

export const EmptyItem = () => (
    <Alert variant="info">
        No contacts yet. Click "Add new" to create one.
    </Alert>
);

const Contact = ({id, name, phones, deleteContact, addPhone}) => (
    <tr>
        <td>
            {id}
        </td>
        <td>
            {name}
        </td>
        <td>
            {phones.map((phone) => phone.number).join(', ')}
        </td>
        <td className="text-right">
            <ButtonGroup size="sm">
                <Link to={`/contact/${id}`} className="btn btn-sm btn-outline-primary">
                    Edit
                </Link>
                <Button variant="outline-success" onClick={() => addPhone(id)}>
                    Add phone
                </Button>
                <Button variant="outline-danger" size="sm" onClick={() => deleteContact(id)}>
                    Del
                </Button>
            </ButtonGroup>
        </td>
    </tr>
);

export default connect(
    null,
    (dispatch) => ({
        deleteContact: (id) => dispatch(deleteContact(id)),
        addPhone: (id) => dispatch(actions.showAddPhoneModal(id))
    })
)(Contact);
