import React from 'react';

import Table from 'react-bootstrap/Table';

export default ({children}) => (
    <Table hover size="sm">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Phone</td>
                <td>&nbsp;</td>
            </tr>
        </thead>

        <tbody>
            {children}
        </tbody>
    </Table>
);
